import "./styles/jobFilter.css";

const JobFilter = ({job, filters}) => {
    const handleOnClickDelete = (e) => {
        const {filter, setFilter} = filters;
        setFilter(filter.filter(el => el !== job));
    }

    return (
        <div className="filter">
            <div className="nameFilter">
                <span>{job}</span>
            </div>
            <div className="deleteFilter" onClick={handleOnClickDelete}>
                <img src="/images/icon-remove.svg" alt={`Delete filter ${job}`}></img>
            </div>
        </div>
    )
}

export default JobFilter;