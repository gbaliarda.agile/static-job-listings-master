import './styles/job.css'

const Job = ({company, logo, isNew, isFeatured, position, role, level, postedAt, contract, location, languages, tools, filters}) => {

    const handleOnClickFilter = (e) => {
        const {filter, setFilter} = filters;
        if(!filter.includes(e.target.innerText))
            setFilter([...filter,e.target.innerText]);
    }

    return (
            <div className={"job-content " + (isFeatured && "featured")}>
                <div className="left-side">
                    <img src={logo} alt={company} />
                    <div className="description">
                        <div className="job-header">
                            <div className="company">
                                {company}
                            </div>
                            <div className="tags">
                                { isNew &&
                                    <div className="tag-new">
                                        <span>NEW!</span>
                                    </div>
                                }
                                { isFeatured &&
                                    <div className="tag-featured">
                                        FEATURED
                                    </div>
                                }
                            </div>
                        </div>
                        <div className="title">
                            {position}
                        </div>
                        <div className="job-footer">
                            <span className="timestamp">
                                {postedAt}
                            </span>
                            <span className="separator"></span>
                            <span className="contract">
                                {contract}
                            </span>
                            <span className="separator"></span>
                            <span className="location">
                                {location}
                            </span>

                        </div>
                    </div>
                </div>
                <hr className="mobile-separator"></hr>
                <div className="right-side">
                    <div className="tags" >
                        { [role, level, ...languages, ...tools].map(el => 
                            <div key={el} className="tag" onClick={handleOnClickFilter}>
                                <span>{el}</span>
                            </div>
                        )}
                    </div>
                </div>
            </div>
    );
}

export default Job;