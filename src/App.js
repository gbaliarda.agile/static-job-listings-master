import './styles/App.css';
import Job from './components/job';
import JobFilter from './components/jobFilter';
import DataJson from "./data.json";
import { useEffect, useState } from "react";

const initialData = DataJson;

function App() {
	const [data, setData] = useState(initialData);
	const [filter, setFilter] = useState([]);


	const handleClearAllFilter = () => {
		setFilter([]);
	}

	useEffect(() => {
		setData(filter.length === 0 ? initialData : initialData.filter(el => filter.every(f => [el.role, el.level, ...el.languages, ...el.tools].includes(f))));
	}, [filter]);

	return (
		<div className="app">
			<div className="header">
				<img src="images/bg-header-desktop.svg" alt="bg_header" className="desktop-bg" />
				<img src="images/bg-header-mobile.svg" alt="bg_header" className="mobile-bg" />
			</div>
				<div className="content-container">
					<div className="content">
						{filter.length > 0 &&
							<div className="filters-container">
								<div className="filters">
									{filter.map(f => { return (
										<JobFilter key={f} job={f} filters={{filter, setFilter}} />
									)})}
								</div>
								<div className="filters-clear">
									<span onClick={handleClearAllFilter}>Clear</span>
								</div>
							</div>
						}
						<div
							className="jobs">
							{ data.map((el, index) => { 
								return (
									<Job
										key={el.id}
										company={el.company}
										logo={el.logo}
										isNew = {el.new}
										isFeatured = {el.featured}
										position = {el.position}
										postedAt = {el.postedAt}
										contract = {el.contract}
										location = {el.location}
										role = {el.role}
										level = {el.level}
										languages= {el.languages}
										tools = {el.tools}
										filters = {{filter: filter, setFilter: setFilter}}
										/>
								);
							})}
						</div>
					</div>
				</div>
			<div className="footer">

			</div>
		</div>
  	);
}

export default App;
